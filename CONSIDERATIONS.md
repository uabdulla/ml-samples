# Considerations for use cases, solutions and sample data

**Clarity** and **conciseness** in the descriptions of the use cases, the data 
and solutions provided is of the highest priority. 

## For use cases

Complexity that would arise in a real-world occurrence of the problem can be 
mentioned and explained. However, a line needs to be drawn so as to keep the 
solution to the problem simple.

### For example:
One could possibly use the same data to predict when an event 
like the next furniture sale will occur and also how cheap various items in 
that sale would be. However, doing both in the same example would result in a
more complex and less understandable solution.  

## For solutions

Similarly, technical considerations (e.g. performance, generalisability, 
robustness) in the solutions given should be explained, but the 
implementation needs to take clarity into consideration first.

### For example:
If choosing between two algorithms that achieve the same result, 
one of which is simpler to explain than the other, the simpler algorithm 
(even if less efficient) should be selected.  The more complex algorithm should
be mentioned and if possible links given for further reading.

## For data

The same priorities extend over sample data used in the solutions. The 
sample data needs to be clear and concise.

### For example:
Additional data that is not related to the use case should not be removed 
from the sample data stored in the repository and not filtered out in the 
code. It should be clear what the data represents, it's units, what
pre-processing (if any) has been performed on it and any known sources of
noise and inaccuracy should be explained. Finally, proper references must be
made to the sources of the data in case one wants to perform further 
investigation.


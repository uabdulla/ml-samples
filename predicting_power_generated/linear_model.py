import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression

csv = pd.read_csv('utah.csv', sep='\t')

# Merge the year and month into a single column called month index
csv['MONTH_INDEX'] = csv.YEAR * 12 + csv.MONTH - 1

# Convert month index and power generated to numpy arrays
loaded_x = csv.MONTH_INDEX.as_matrix()
loaded_y = csv.GENERATION.as_matrix()

# Change the shapes of the arrays to 2D arrays
loaded_x = loaded_x.reshape(loaded_x.shape[0], 1)
loaded_y = loaded_y.reshape(loaded_y.shape[0], 1)

# Change the data from Megawatt-hours to Gigawatt-hours
loaded_y //= 1000

# Take 10% of the data and use it for testing, leave the remaining for training
testing_size = loaded_x.shape[0] // 10
training_x = loaded_x[:-testing_size]
training_y = loaded_y[:-testing_size]
testing_x = loaded_x[-testing_size:]
testing_y = loaded_y[-testing_size:]

lr = LinearRegression(normalize=True)

# Fit the linear model
model = lr.fit(training_x, training_y)

# noinspection PyTypeChecker
root_mean_sq_error = np.mean((model.predict(testing_x) - testing_y) ** 2) \
                     ** 0.5
score = model.score(testing_x, testing_y)

# The mean squared error
# noinspection PyStringFormat
print("Root mean squared error: %.2f" % root_mean_sq_error)
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % score)

# Create a figure and plot out the data
fig = plt.figure()
fig.suptitle('Power Generated (Gigawatt Hours)', fontsize=14, fontweight='bold')

training_data_plot = plt.scatter(training_x,
                                 training_y,
                                 marker='.',
                                 color='black',
                                 label='Training data set')

trained_model, = plt.plot(training_x,
                          model.predict(training_x),
                          color='black',
                          label='Model from training data')

testing_data_plot = plt.scatter(testing_x,
                                testing_y,
                                marker='.',
                                color='blue',
                                label='Testing data set')

extended_model, = plt.plot(testing_x,
                           model.predict(testing_x),
                           color='blue',
                           label='Model extended over training data')

plt.legend(handles=[training_data_plot, trained_model,
                    testing_data_plot, extended_model])

x_tick_values = [x[0] for x in filter(lambda v: v % 12 == 0, loaded_x)]
x_tick_labels = ["%d" % (x / 12) for x in x_tick_values]
plt.xticks(x_tick_values, x_tick_labels, rotation='vertical')

plt.show()
